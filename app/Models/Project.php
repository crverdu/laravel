<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];
    use HasFactory;

    /**
     * Reescritura para el Route
     * @return $url del proyecto
     */
    public function getRouteKeyName()
    {
        return ('url');
    }
}
