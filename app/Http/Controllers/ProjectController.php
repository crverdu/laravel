<?php

namespace App\Http\Controllers;


use App\Models\Project;
use App\Http\Requests\SaveProjectRequest;

class ProjectController extends Controller
{
    /**
     * Mostrar un listado de los Proyectos.
     *
     *  @return app\resources\views\projects\index
     */
    public function index()
    {
        $proyectos = Project::latest()->get();
        return view('projects.index',compact('proyectos'));
    }

    /**
     * Mostrar un formulario para crear un nuevo Proyecto.
     *
     * @return app\resources\views\projects\create
     */
    public function create()
    {
        return view('projects.create',[
            'proyecto' => new Project
        ]);
    }

    /**
     * Guardar un proyecto creado.
     * @param App\Http\Requests\SaveProjectRequest $request
     * @return app\resources\views\projects\index
     */
    public function store( SaveProjectRequest $request ){
        Project::create($request->validated());
        return redirect()->route('projects.index');
    }

    /**
     * Mostrar un Proyecto especifico.
     *
     * @param  Project  $project
     * @return app\resources\views\projects\show
     */
    public function show(Project $project)
    {
        return view('projects.show',[
            'proyecto' => $project
        ]);
    }

    /**
     * Show the form for editing the specified Proyecto.
     *
     * @param  Project  $project
     * @return app\resources\views\projects\edit
     */
    public function edit(Project $project)
    {
        return view('projects.edit',[
            'proyecto' => $project
        ]);
    }

    /**
     * Update the specified Proyecto in storage.
     *
     * @param  App\Http\Requests\SaveProjectRequest  $request
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, SaveProjectRequest $request)
    {
        $project->update($request->validated());
        return redirect()->route('projects.show',$project);
    }

    /**
     * Remove the specified Proyecto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route('projects.index');
    }
}
