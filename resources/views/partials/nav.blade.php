<nav>
    <ul>
        <li class={{ setActive('index') }}> <a href="{{route('index')}}">Home </a> </li>
        <li class={{ setActive('about') }}> <a href="{{route('about')}}">Acerca de... </a> </li>
        <li class={{ setActive('projects.*') }}> <a href="{{route('projects.index')}}">Portafolio </a> </li>
        <li class={{ setActive('contacto') }}> <a href="{{route('contacto')}}">Contacto </a> </li>
    </ul>
</nav>
