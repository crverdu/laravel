@extends('layout')
@section('titulo')
    Crear Proyecto
@endsection
@section('body')
    <h2>Crear un nuevo Proyecto</h2>
    @include('partials.validation-errors')
    <form action="{{route('projects.store')}}" method="post">
        @include('projects._form',['btnText'=>'Guardar'])
    </form>
@endsection
