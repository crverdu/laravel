@extends('layout')
@section('titulo')
    Porfolio
@endsection
@section('body')
    <h2>Portfolio</h2>
    <a href="{{ route('projects.create') }}">Crear Proyecto</a>
    <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil nam eligendi voluptate dolor excepturi fugit incidunt, quo ipsam nulla. Voluptatibus rem labore necessitatibus ab accusantium distinctio similique provident dolorum voluptas!</p>

    <ul>
        @if ($proyectos)
            @foreach ( $proyectos as $proyecto)
            <li>
                <a href="{{ route ('projects.show',$proyecto) }}">{{$proyecto->title}}</a>
            </li>
            @endforeach
        @else
        <li>
            No hay elementos disponibles
        </li>
        @endif

    </ul>
@endsection
