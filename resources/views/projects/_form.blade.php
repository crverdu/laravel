@csrf
<label>
    Titulo del proyecto
    <input type="text" name="title" value="{{ old('title', $proyecto->title)}}" >
</label> <br>
<label>
    URL del proyecto
    <input type="text" name="url" value="{{ old ('url', $proyecto->url)}}">
</label> <br>
<label>
    Descripcion del proyecto
    <textarea name="description"  cols="30" rows="10">{{ old('description', $proyecto->description)}}</textarea>
</label> <br>
<button>{{$btnText}}</button>
