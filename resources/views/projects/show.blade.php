@extends('layout')
@section('titulo')
    Portafolio | {{$proyecto->title}}
@endsection
@section('body')
    <h1>{{$proyecto->title}}</h1>
    <a href="{{route ('projects.edit',$proyecto)}}">Editar</a>
    <form action="{{route ('projects.destroy',$proyecto)}}" method="post">
        @csrf @method('DELETE')
        <button type="submit">Eliminar</button>
    </form>
    <p>{{$proyecto->description}}</p>
    <small>creado: {{$proyecto->created_at->format('d-m-Y H:m')}}</small> <br>
    <small>modificado: {{$proyecto->updated_at->format('d-m-Y H:m')}}</small>
@endsection
