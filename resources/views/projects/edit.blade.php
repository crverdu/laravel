@extends('layout')
@section('titulo')
    Crear Proyecto
@endsection
@section('body')
    <h2>Editar Proyecto</h2>
    @include('partials.validation-errors')
    <form action="{{route('projects.update',$proyecto)}}" method="post">
        @method('patch')
        @include('projects._form',['btnText'=>'Actualizar'])
    </form>
@endsection
