<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail</title>
</head>
<body>
    <h1>Nuevo Correo Web</h1>
    <h2>Recibiste un mensaje de: {{$mensaje['name']}} - {{$mensaje['email']}} </h2>
    <h4><strong>Asunto: {{$mensaje['subject']}}</strong></h4>
    <p>Contenido: {{$mensaje['content']}}</p>


</body>
</html>
