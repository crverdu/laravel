<!DOCTYPE html>
<html>

<head>
    <title>
        @yield('titulo','verdus')
    </title>
    <style>
        .active a{
            color: red;
            text-decoration: none;
        }
    </style>
</head>

<body>
    @include('partials.nav')
    @yield('body')
</body>

</html>
